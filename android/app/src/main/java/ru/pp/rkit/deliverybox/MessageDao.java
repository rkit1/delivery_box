package ru.pp.rkit.deliverybox;
import androidx.room.*;

import java.util.List;

@Dao
public interface MessageDao {
    @Query("SELECT * FROM message WHERE phone_number = :number ORDER BY id DESC ") // ORDER BY
    List<Message> getAll(String number);

    @Query("SELECT * FROM message WHERE phone_number = :number ORDER BY id DESC LIMIT :count") // ORDER BY
    Message[] getAll(String number, int count);

    @Insert
    void insertAll(Message... messages);
}
