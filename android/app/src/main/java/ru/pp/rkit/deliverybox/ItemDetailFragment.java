package ru.pp.rkit.deliverybox;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

//import ru.pp.rkit.deliverybox.dummy.DummyContent;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private DeliveryBox mItem;

    private App app;

    private BroadcastReceiver sms_update_receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            refresh_log();
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    private RecyclerView log_recycler;
    private RecyclerView.Adapter log_recycler_adapter;
    private RecyclerView.LayoutManager log_recycler_manager;

    void refresh_log() {
        Message[] data = app.db.messageDao().getAll(mItem.phone_number, 300);
        log_recycler_adapter = new LogEntryAdapter(data);
        log_recycler.setAdapter(log_recycler_adapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (App)getContext().getApplicationContext();
        if (getArguments().containsKey(ARG_ITEM_ID)) {
            mItem = app.db.deliveryBoxDao().get(getArguments().getString(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.description);
            }
        }
        getActivity().registerReceiver(sms_update_receiver, new IntentFilter("sms_update"));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(sms_update_receiver);
    }

    void generate_code() {
        int random = new Random().nextInt(10000);
        View view = getView();
        assert (view != null);
        EditText code_input = (EditText) view.findViewById(R.id.code_input);
        assert (code_input != null);
        code_input.setText(String.format("%04d", random));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);
        if (mItem != null) {
        }
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageButton generate_new_code = (ImageButton) view.findViewById(R.id.generate_new_code);
        assert (generate_new_code != null);
        generate_new_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generate_code();
            }
        });
        generate_code();

        final EditText code_input = (EditText) view.findViewById(R.id.code_input);
        assert (code_input != null);

        TextView phone_number = view.findViewById(R.id.phone_number);
        phone_number.setText(mItem.phone_number);

        Button box_set_code_button = (Button) view.findViewById(R.id.box_set_code_button);
        assert (box_set_code_button != null);
        box_set_code_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.send_box_code(mItem, code_input.getText().toString());
                generate_code();
            }
        });

        log_recycler = (RecyclerView) view.findViewById(R.id.log_recycler);
        log_recycler.setHasFixedSize(true);

        // use a linear layout manager
        log_recycler_manager = new LinearLayoutManager(this.getContext());
        log_recycler.setLayoutManager(log_recycler_manager);

        refresh_log();
    }

}


