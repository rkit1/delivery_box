package ru.pp.rkit.deliverybox;
import android.telephony.SmsMessage;

import androidx.annotation.NonNull;
import androidx.room.*;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity(foreignKeys = @ForeignKey(entity = DeliveryBox.class,
        parentColumns = "phone_number",
        childColumns = "phone_number",
        onDelete = ForeignKey.CASCADE),
        indices = {@Index("time")}
        )
public class Message {
    private static Pattern code_set_pattern = Pattern.compile("CODE IS (?<code>\\d{4})");
    private static Pattern door_status_pattern = Pattern.compile("BOX (?<doorstatus>(OPENED (CODE|NO CODE))|(CLOSED (FULL|EMPTY))) BATTERY (?<batterystatus>\\d+)");

    public enum Direction {
        direction_out,
        direction_in
    }

    public enum MessageType {
        type_unknown,
        type_code_accepted,
        rype_door_status_changed,
    }

    public Message(){};
    public Message(@NonNull SmsMessage msg, @NonNull Direction direction) {
        String num = msg.getOriginatingAddress();
        assert (num != null);
        phone_number = num;
        message = msg.getMessageBody();
        this.direction = direction;
    }

    public void update_box(@NonNull AppDatabase db, @NonNull DeliveryBox box) {
        Matcher m = code_set_pattern.matcher(message);
        if (m.matches()) {
            box.code = m.group(1);
            db.deliveryBoxDao().update(box);
            return;
        }
        m = door_status_pattern.matcher(message);
        if (m.matches()) {
            if (m.group(1).equals("CLOSED FULL")) {
                box.door_status = DeliveryBox.DoorStatus.closed;
                box.content_status = true;
            } else if (m.group(1).equals("CLOSED EMPTY")) {
                box.door_status = DeliveryBox.DoorStatus.closed;
                box.content_status = false;
            } else if (m.group(1).equals("OPENED CODE")) {
                box.door_status = DeliveryBox.DoorStatus.opened_with_code;
                box.code = null;
            } else {
                box.door_status = DeliveryBox.DoorStatus.opened_no_code;
            }
            box.battery_charge = Integer.parseInt(m.group(6));
            db.deliveryBoxDao().update(box);
            return;
        }

    }

    @PrimaryKey(autoGenerate = true)
    public int id;

    @NonNull
    public String phone_number;

    @NonNull
    public Date time = new Date();

    @NonNull
    public String message;

    @NonNull
    public Direction direction;

    @NonNull
    public MessageType message_type = MessageType.type_unknown;
}
