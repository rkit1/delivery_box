package ru.pp.rkit.deliverybox;
import android.telephony.SmsManager;

import androidx.annotation.NonNull;
import androidx.room.*;

import java.util.Locale;

@Entity
public class DeliveryBox {
    enum DoorStatus {
        closed,
        opened_with_code,
        opened_no_code,
    };


    @PrimaryKey
    @NonNull
    public String phone_number;

    public String description;

    public DeliveryBox(@NonNull String phone_number, String description) {
        this.phone_number = phone_number;
        this.description = description;
    }



    public boolean content_status;
    public String code;
    @NonNull
    public DoorStatus door_status = DoorStatus.closed;
    public int battery_charge = 0;
}


