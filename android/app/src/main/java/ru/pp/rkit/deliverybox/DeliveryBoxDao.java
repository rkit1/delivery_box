package ru.pp.rkit.deliverybox;
import androidx.annotation.NonNull;
import androidx.room.*;

import java.util.List;

@Dao
public interface DeliveryBoxDao {
    @Query("SELECT * FROM deliverybox")
    List<DeliveryBox> getAll();

    @Query("SELECT * FROM deliverybox WHERE phone_number = :phone_number")
    DeliveryBox get(String phone_number);

    @Update()
    void update(DeliveryBox box);

    @Query("UPDATE deliverybox SET description = :description WHERE phone_number = :phone_number")
    void update_description(@NonNull String phone_number, @NonNull String description);

    @Insert
    void insertAll(DeliveryBox... boxes);

    @Delete
    void delete(DeliveryBox box);

    @Query("DELETE FROM deliverybox WHERE phone_number = :phone_number")
    void deleteByPhoneNumber(String phone_number);
}
