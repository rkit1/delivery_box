package ru.pp.rkit.deliverybox;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class LogEntryAdapter extends RecyclerView.Adapter<LogEntryAdapter.MyViewHolder> {
    private Message[] mDataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ViewGroup layout;
        public MyViewHolder(ViewGroup l) {
            super(l);
            layout = l;
        }
    }

    public LogEntryAdapter(Message[] myDataset) {
        mDataset = myDataset;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewGroup v = (ViewGroup) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.log_list_entry, parent, false);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Message data = mDataset[position];

        TextView time = (TextView)holder.layout.findViewById(R.id.time);
        time.setText(DateFormat.format("dd.MM hh:mm:ss", data.time));
        TextView message = (TextView)holder.layout.findViewById(R.id.message);
        message.setText(data.message);
        TextView direction = (TextView)holder.layout.findViewById(R.id.direction);
        direction.setText(
                data.direction == Message.Direction.direction_in
                        ? R.string.message_log_direction_in
                        : R.string.message_log_direction_out
                );
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

}
