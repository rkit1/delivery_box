package ru.pp.rkit.deliverybox;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import androidx.room.Room;

public class SmsReceiver extends BroadcastReceiver {
    private static final String TAG =
            SmsReceiver.class.getSimpleName();
    public static final String pdu_type = "pdus";

    @Override
    public void onReceive(Context context, Intent intent) {
        // Get the SMS message.
        Bundle bundle = intent.getExtras();
        //String format = bundle.getString("format");
        Object[] pdus = (Object[]) bundle.get(pdu_type);
        if (pdus != null) {
            AppDatabase db = Room.databaseBuilder(context,
                    AppDatabase.class, "database-name").allowMainThreadQueries().build();
            // Fill the msgs array.
            for (Object o : pdus) {
                SmsMessage msg = SmsMessage.createFromPdu((byte[]) o);
                if (msg.getOriginatingAddress() == null) continue;
                DeliveryBox box = db.deliveryBoxDao().get(msg.getOriginatingAddress());
                if (box != null) {
                    Message msg_my = new Message(msg, Message.Direction.direction_in);
                    msg_my.update_box(db, box);
                    db.messageDao().insertAll(msg_my);
                    context.sendBroadcast(new Intent("sms_update"));
                } else if (msg.getMessageBody().equals("BOX OWNER ACTIVATED")) {
                    box = new DeliveryBox(msg.getOriginatingAddress(), msg.getOriginatingAddress());
                    db.deliveryBoxDao().insertAll(box);
                    context.sendBroadcast(new Intent("sms_update"));
                }
            }
        }
    }
}
