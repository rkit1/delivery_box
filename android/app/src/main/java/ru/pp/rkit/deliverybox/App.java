package ru.pp.rkit.deliverybox;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import androidx.annotation.NonNull;
import androidx.room.Room;
import java.util.Locale;

public class App extends Application {
    AppDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();
        db = Room.databaseBuilder(this,
                AppDatabase.class, "database-name").allowMainThreadQueries().build();
    }

    public void send_sms(@NonNull Message msg) {
        SmsManager.getDefault().sendTextMessage(msg.phone_number,
                null,
                msg.message,
                null,
                null);
        db.messageDao().insertAll(msg);
        sendBroadcast(new Intent("sms_update"));
    }

    public void send_box_code(@NonNull DeliveryBox box, @NonNull String code) {
        Message msg = new Message();
        msg.message = String.format(Locale.ENGLISH, "CODE %s", code);
        msg.phone_number = box.phone_number;
        msg.direction = Message.Direction.direction_out;
        send_sms(msg);
    }
}
