package ru.pp.rkit.deliverybox;
import androidx.room.*;

import java.util.Date;

@Database(entities = {DeliveryBox.class, Message.class}, version = 1,
        exportSchema = false // ???
)
@TypeConverters({AppDatabase.Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract DeliveryBoxDao deliveryBoxDao();
    public abstract MessageDao messageDao();

    static class Converters {
        @TypeConverter
        public static Date fromTimestamp(Long value) {
            return value == null ? null : new Date(value);
        }

        @TypeConverter
        public static Long dateToTimestamp(Date date) {
            return date == null ? null : date.getTime();
        }


        @TypeConverter
        public static Message.Direction fromInt(int value) {
            return value == 0 ? Message.Direction.direction_in : Message.Direction.direction_out;
        }

        @TypeConverter
        public static int toInt(Message.Direction value) {
            return value == Message.Direction.direction_in ? 0 : 1;
        }


        @TypeConverter
        public static Message.MessageType messageTypeFromInt(int value) {
            return Message.MessageType.values()[value];
        }

        @TypeConverter
        public static int messageTypeToInt(Message.MessageType value) {
            return value.ordinal();
        }


        @TypeConverter
        public static DeliveryBox.DoorStatus doorStatusTypeFromInt(int value) {
            return DeliveryBox.DoorStatus.values()[value];
        }

        @TypeConverter
        public static int doorStatusTypeToInt(DeliveryBox.DoorStatus value) {
            return value.ordinal();
        }
    }
}



