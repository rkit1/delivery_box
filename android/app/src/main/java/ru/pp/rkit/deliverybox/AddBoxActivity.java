package ru.pp.rkit.deliverybox;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Pattern;

import ru.pp.rkit.deliverybox.databinding.ActivityAddBoxBinding;

public class AddBoxActivity extends AppCompatActivity {
    Button button;
    EditText phone_number, box_title;
    App app;
    ValidationVM vm;

    public static class ValidationVM extends BaseObservable {
        static Pattern phone = Pattern.compile("^\\+[0-9]{10,13}$");

        @Bindable
        public String getPhone_number() {
            return box.phone_number;
        }

        public void setPhone_number(String phone_number) {
            if (!this.box.phone_number.equals(phone_number)) {
                if (phone_number.length() <= 1) phone_number = "+";
                this.box.phone_number = phone_number;
                notifyPropertyChanged(BR.phone_number);
                notifyPropertyChanged(BR.valid);
            }
        }

        @Bindable
        public String getBox_title() {
            return box.description;
        }

        public void setBox_title(String box_title) {
            if (!this.box.description.equals(box_title)) {
                this.box.description = box_title;
                notifyPropertyChanged(BR.box_title);
                notifyPropertyChanged(BR.valid);
            }
        }

        @Bindable
        public boolean getValid() {
            return this.box.description.length() > 3 &&
                phone.matcher(this.box.phone_number).matches();
        }

        @Bindable
        public boolean getPhone_enabled() {
            return !this.edit_mode;
        }

        @Bindable
        public String getButton_text() {
            return edit_mode ? "сохранить" : "добавить";
        }

        boolean edit_mode = false;
        DeliveryBox box = new DeliveryBox("+7", "");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (App) getApplicationContext();

        final ValidationVM vm = new ValidationVM();
        String edit_phone_number = getIntent().getStringExtra("edit_phone_number");
        if (edit_phone_number != null) {
            vm.box = app.db.deliveryBoxDao().get(edit_phone_number);
            vm.edit_mode = true;
        }
        ActivityAddBoxBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_add_box);
        binding.setVm(vm);

        button = findViewById(R.id.button);
        phone_number = findViewById(R.id.phone_number);
        box_title = findViewById(R.id.box_title);



        //phone_number.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vm.edit_mode) {
                    app.db.deliveryBoxDao().update_description(vm.box.phone_number, vm.box.description);
                } else {
                    app.db.deliveryBoxDao().insertAll(vm.box);
                }
                startActivity(new Intent(AddBoxActivity.this, ItemListActivity.class));
            }
        });

    }
}