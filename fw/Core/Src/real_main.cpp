#include <change_control_number.hpp>
#include <control_variables.hpp>
#include <IR.hpp>
#include "adc_group_manager.hpp"
#include "real_main.h"
#include "stm32_tm1637.h"
#include "stm32f0xx_hal.h"
#include <Serial.hpp>
#include "keypad.h"
#include "timer.hpp"
#include <algorithm>
#include "IR.hpp"
#include "SIM800.hpp"
#include "change_control_number.hpp"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern ADC_HandleTypeDef hadc;

SerialClass Serial1(&huart1);
SerialClass Serial2(&huart2);

GPIO Btn1 = _GPIO(B1);
GPIO box_open = _GPIO(BOX_OPEN);
GPIO lock_open = _GPIO(LOCK_OPEN);
GPIO admin_btn = _GPIO(ADMIN_BTN);

SIM800 sim(Serial1);

ADC_group battery_channel(std::vector<uint8_t>({{ADC_CHANNEL_11}}), ADC_RESOLUTION_12B);
uint8_t battery_level() {
	static Timer update_timer;
	static uint8_t value_memo;
	constexpr int samples = 3;
	constexpr uint32_t min_charge = 2916;
	constexpr uint32_t max_charge = 3475;
	// делитель 180 : 39
	// 13,2 = 2,35 = 2916
	// 16,8 = 3 = 3723
	// 16,2 = 2.8 = 3475

	if (update_timer.done()) {
		battery_channel.enable();
		uint32_t value = 0;

		for (auto i = 0; i < samples; i++) {
			HAL_ADC_Start(&hadc);
			HAL_ADC_PollForConversion(&hadc, 1000);
			value += HAL_ADC_GetValue(&hadc);
		}
		value /= samples;

		value -= min_charge;
		if (value < 0) value = 0;
		value = value * 100 / (max_charge - min_charge);
		if (value > 100) value = 100;
		value_memo = value;
		update_timer.start_loop(10 * 60 * 1000);
	}
	return value_memo;
}

void open_lock() {
	lock_open.write(GPIO_PIN_SET);
	HAL_Delay(50);
	lock_open.write(GPIO_PIN_RESET);
}

bool kbd_loop() {
	static int i = 0;
	static std::array<int8_t, 4> digits = {{-1,-1,-1,-1}};
	static Timer box_open_timeout(false);

	auto res = Keypad::read();
	res = Keypad::debounce(res);

	if (box_open_timeout.done()) {
		goto reset;
	} else if (box_open_timeout.running) {
		return true;
	}

	if (res >= 10) {
		goto reset;
	}
	if (res >= 0) {
		digits[i++] = res;
		tm1637DisplayArr(digits);
	}
	if (i == 4) {
		if (std::equal(digits.begin(), digits.end(), keypad_code.begin())) {
			box_open_timeout.start(5000);
			sim.invalidate_code();
			return true;
		}
		else {
			goto reset;
		}
	}

	return false;
	reset:
	box_open_timeout.running = false;
	digits = {{-1,-1,-1,-1}};
	i = 0;
	tm1637DisplayArr(digits);
	return false;
}

void stop_mode() {

	  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
	  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
}

void real_main() {
	tm1637Init();
	tm1637Clear();
	tm1637SetBrightness(3);
	/*uint16_t i = 0;
	while (HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin) != GPIO_PIN_RESET) {
		tm1637DisplayDecimal(i++);
		HAL_Delay(10);
	}*/

	Serial1.begin();
	Serial2.begin();
	HAL_Delay(1000);
	sim.init();

	uint8_t buf[32];
	size_t size;
	static bool kbd_old = false;
	while(true) {
		size = Serial2.read(buf);
		if (size > 0) {
			Serial1.write(buf, size);
		}
		sim.parser_loop();

		auto kbd_success = kbd_loop();
		if (kbd_old != kbd_success) {
			kbd_old = kbd_success;
			if (kbd_old) {
				open_lock();
			}
		}

		if (box_open.front()) {
			HAL_Delay(10); // Антиотскок
			std::string message;
			if (box_open.front_state == GPIO_PIN_RESET) {
				if(IR::check_for_object()) {
					message = "BOX CLOSED FULL ";
				} else {
					message = "BOX CLOSED EMPTY ";
				}
			}
			else if (kbd_old) message = "BOX OPENED CODE ";
			else message = "BOX OPENED NO CODE ";
			message += "BATTERY ";
			message += std::to_string(battery_level());
			sim.send_sms(message);
		}

		if(Btn1.front(GPIO_PIN_RESET)) {
			char buf2[32];
			sprintf(buf2, "%d\n", battery_level());
			Serial2.write(buf2);
		}

		if(box_open.read() == GPIO_PIN_SET && admin_btn.read() == GPIO_PIN_RESET) {
			Change_Control_Number cn;
			auto number = cn.change();
			if (number != "") {
				sim.change_admin_number(number);
				sim.send_sms("BOX OWNER ACTIVATED");
			}
		}
	}
}
