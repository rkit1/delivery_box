#include <Serial.hpp>
#include <cstring>
#include <algorithm>
#include <iterator>

SerialClass::SerialClass(UART_HandleTypeDef * uart): uart(uart) {

}

void SerialClass::transmit(size_t count) {
	HAL_UART_Transmit_DMA(uart, tx_buf.data(), count);
}

void SerialClass::transmit(WriteIterator count) {
	HAL_UART_Transmit_DMA(uart, tx_buf.data(), count - tx_buf.begin());
}

void SerialClass::write_hex(uint32_t n) {
	while (uart->gState != HAL_UART_STATE_READY) {};
	itoa(n, reinterpret_cast<char*>(tx_buf.data()), 16);
	transmit(strlen(reinterpret_cast<char*>(tx_buf.data())));
}

void SerialClass::write(uint32_t n) {
	while (uart->gState != HAL_UART_STATE_READY) {};
	itoa(n, reinterpret_cast<char*>(tx_buf.data()), 10);
	transmit(strlen(reinterpret_cast<char*>(tx_buf.data())));
}

void SerialClass::write(uint16_t n) {
	while (uart->gState != HAL_UART_STATE_READY) {};
	itoa(n, reinterpret_cast<char*>(tx_buf.data()), 10);
	transmit(strlen(reinterpret_cast<char*>(tx_buf.data())));
}

void SerialClass::write(int n) {
	while (uart->gState != HAL_UART_STATE_READY) {};
	itoa(n, reinterpret_cast<char*>(tx_buf.data()), 10);
	transmit(strlen(reinterpret_cast<char*>(tx_buf.data())));
}

void SerialClass::write(const uint8_t* buf, size_t count) {
	while (uart->gState != HAL_UART_STATE_READY) {};
	std::copy_n(buf, count, tx_buf.begin());
	transmit(count);
}

void SerialClass::write(std::string string) {
	while (uart->gState != HAL_UART_STATE_READY) {};
	std::copy(string.begin(), string.end(), tx_buf.begin());
	transmit(string.size());
}


void SerialClass::write(const char * string) {
	while (uart->gState != HAL_UART_STATE_READY) {
		__asm__ __volatile__("nop\n\t":::"memory");
	};
	size_t count = 0;
	for(; string[count] != 0; count++) {
		tx_buf[count] = string[count];
	}
	transmit(count);
}


size_t SerialClass::read(uint8_t* buf) {
	uint16_t reg_count = uart->hdmarx->Instance->CNDTR;
	size_t read_count = 0;
	while(rx_last_count != reg_count) {
		buf[read_count++] = rx_buf[sizeof(rx_buf) - rx_last_count];
		rx_last_count = rx_last_count-1;
		if (rx_last_count == 0) rx_last_count = sizeof(rx_buf);
	}
	return read_count;
}

std::string SerialClass::read() {
	auto end_it = readEnd();
	auto str = std::string(readBegin(), end_it);
	end_it.commit();
	return str;
}

std::string SerialClass::readUntil(const uint8_t val) {
	auto it = std::find(readBegin(), readEnd(), val);
	if (*it != val) return "";
	auto str = std::string(readBegin(), ++it);
	it.commit();
	return str;
}

std::string SerialClass::readUntil(const char* val) {
	auto end = readEnd();
	auto it = std::search(readBegin(), end, val, val + strlen(val));
	if (it == end) return "";

	++it; ++it;
	auto str = std::string(readBegin(), it);
	it.commit();
	return str;
}

void SerialClass::begin() {
	//__HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
	//__HAL_UART_ENABLE_IT(&huart1, UART_IT_TC);
	//__HAL_UART_DISABLE_IT(&uart, UART_IT_IDLE); // IDLE LINE

	HAL_UART_Receive_DMA(uart, rx_buf.data(), rx_buf.size());
	//

}

SerialClass::ReadIterator SerialClass::readBegin() {
	return ReadIterator(*this, rx_last_count);
}

SerialClass::ReadIterator SerialClass::readEnd() {
	return ReadIterator(*this, uart->hdmarx->Instance->CNDTR);
}

SerialClass::WriteIterator SerialClass::writeBegin() {
	while (uart->gState != HAL_UART_STATE_READY) {};
	return tx_buf.begin();
}

SerialClass::WriteIterator SerialClass::writeEnd() {
	return tx_buf.end();
}

SerialClass::ReadIterator::ReadIterator(SerialClass& parent, size_t rx_last_count)
	: parent(parent), it_rx_last_count(rx_last_count) {}

bool SerialClass::ReadIterator::operator==(ReadIterator other) {
	return (it_rx_last_count == other.it_rx_last_count);
}

bool SerialClass::ReadIterator::operator!=(ReadIterator other) {
	return !(*this == other);
}

SerialClass::ReadIterator& SerialClass::ReadIterator::operator++() {
	it_rx_last_count = it_rx_last_count-1;
	if (it_rx_last_count == 0) it_rx_last_count = sizeof(rx_buf);
	return *this;
}

SerialClass::ReadIterator SerialClass::ReadIterator::operator++(int) {
	auto ret = *this;
	++*this;
	return ret;
}

SerialClass::ReadIterator& SerialClass::ReadIterator::operator=(SerialClass::ReadIterator other) {
	it_rx_last_count = other.it_rx_last_count;
	return *this;
}

void SerialClass::ReadIterator::commit() {
	parent.rx_last_count = it_rx_last_count;
}

uint8_t SerialClass::ReadIterator::operator*() const {
	return parent.rx_buf[sizeof(rx_buf) - it_rx_last_count];
}

//extern UART_HandleTypeDef huart1;
//SerialClass Serial(&huart1);
