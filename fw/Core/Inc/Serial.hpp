#pragma once
#include <stdint.h>
#include <stm32f0xx_hal.h>
#include <string>
#include <iterator>
#include <array>

class SerialClass {
	static constexpr size_t tx_buf_size = 64;
	static constexpr size_t rx_buf_size = 512;

	std::array<uint8_t, tx_buf_size> tx_buf;
	std::array<uint8_t, rx_buf_size> rx_buf;
	size_t rx_last_count = rx_buf.size();
	UART_HandleTypeDef * uart;
public:
	class ReadIterator: public std::iterator<std::forward_iterator_tag, uint8_t> {
	protected:
		SerialClass& parent;
		size_t it_rx_last_count;
		ReadIterator(SerialClass& parent, size_t rx_last_count);
	public:
		ReadIterator& operator++();
		ReadIterator operator++(int);
		bool operator==(ReadIterator other);
		bool operator!=(ReadIterator other);
		ReadIterator& operator=(ReadIterator other);
		uint8_t operator*() const;
		void commit();
	friend SerialClass;
	};
	using WriteIterator = std::array<uint8_t, tx_buf_size>::iterator;

	void transmit(size_t count);
	void transmit(WriteIterator it);
	size_t read(uint8_t* buf);
	std::string read();
	std::string readUntil(const uint8_t val);
	std::string readUntil(const char* val);
	ReadIterator readBegin();
	ReadIterator readEnd();
	WriteIterator writeBegin();
	WriteIterator writeEnd();
	void write(const uint8_t* buf, size_t count);
	void write(const char * string);
	void write(std::string string);
	void write(uint16_t n);
	void write(uint32_t n);
	void write_hex(uint32_t n);
	void write(int n);
	void begin();
	SerialClass(UART_HandleTypeDef * uart);
};




extern SerialClass Serial;
