#pragma once
#include <control_variables.hpp>
#include "stdint.h"
#include "Keypad.h"
#include "stm32_tm1637.h"

class Change_Control_Number {
	std::array<int8_t, 15> digits;
	int i = 0;

	void display() {
		if (i > 0) {
			std::array<int8_t, 4> display_digits = {{16, 16, 16, 16}};
			int8_t begin_i = std::max(i - 3, 0);
			std::copy(digits.begin() + begin_i, digits.begin() + i, display_digits.begin());
			tm1637DisplayArr(display_digits);
		}
		else {
			tm1637DisplayArr({15,15,15,15});
		}
	}

	void reset() {
		//digits.fill(-1);
		i = 0;
		display();
	}

	bool query_commit() {
		std::array<int8_t, 4> display_digits;
		int8_t slice_i = -3;
		Timer t(0ul);
		while(true) {
			auto res = Keypad::read();
			res = Keypad::debounce(res);
			if (res == Keypad::key_enter) {
				return true;
			}
			else if (res == Keypad::key_reset) {
				return false;
			}
			if (t.done()) {
				t.start_loop(600);
				for (uint8_t k = 0; k < 4; k++) {
					int8_t j = slice_i + k;
					display_digits[k] = (j < 0 || j >= i) ? 16 : digits[j];
				}
				tm1637DisplayArr(display_digits);
				slice_i++;
				if (slice_i > i) slice_i = -3;
			}
		}
	}

public:
	std::string change() {
		display();
		while(true) {
			auto res = Keypad::read();
			res = Keypad::debounce(res);
			if (res >= 0) {
				if (res == Keypad::key_enter && i > 0) {
					if (query_commit()) {
						std::string out = "+";
						out.reserve(12);
						for (auto j = 0; j < i; j++) {
							out += std::to_string(digits[j]);
						}
						tm1637DisplayArr({10,10,10,10});
						return out;
					} else {
						reset();
					}
				}
				else if (res == Keypad::key_reset) {
					if (i == 0) {
						tm1637DisplayArr({16,16,16,16});
						return "";
					}
					reset();
				}
				else if (res <= 9) {
					digits[i++] = res;
					display();
					if (i >= (int8_t)digits.size()) reset();
				}
			}
		}
	}
};
