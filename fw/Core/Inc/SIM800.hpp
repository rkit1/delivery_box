#pragma once
#include <control_variables.hpp>
#include "Serial.hpp"
#include <string>
#include <set>
#include <queue>

//extern SerialClass Serial1;
extern SerialClass Serial2;
extern GPIO Btn1;
GPIO reset = _GPIO(SIM800_RESET);

class SIM800 {
	SerialClass & serial;
	enum {
		start,
		waiting_for_sms
	} parser_state = start;
	Timer timeout;
	Timer comm_timeout;
	using sms_id = uint8_t;
	std::set<sms_id> delete_list;
	enum command_type: uint8_t {
		command_code = 0,
		command_type_max
	};
	std::array<sms_id, command_type_max> active_commands = {255};

	std::queue<std::string> outgoing_sms;

	struct Incoming_SMS {
		sms_id storage_number;
		std::string phone_number;
		std::string time;
		std::string line;
		void print() {
			char buf[64];
			sprintf(buf, "SMS_print %d %s %s %.15s\r\n", storage_number, phone_number.c_str(), time.c_str(), line.c_str());
			buf[sizeof(buf)] = 0;
			Serial2.write(buf);
		}
	} temp_sms;

	void _send_sms(const std::string & message) {
		if (sms_debug) {
			Serial2.write("SMS: ");
			Serial2.write(message);
			Serial2.write("\r\n");
		} else {
			char buf[65];
			sprintf(buf, "AT+CMGS=\"%s\"\r\n", control_number.c_str());
			serial.write(buf);
			HAL_Delay(100);
			serial.write(message);
			HAL_Delay(100);
			serial.write("\x1A");
			await_line("OK\r\n");
		}
	}

	void _delete_sms(sms_id index) {
		char buf[32];
		sprintf(buf, "AT+CMGD=%d,0\r\n", index);
		serial.write(buf);
	}
public:
	SIM800(SerialClass & serial): serial(serial) {

	}

	void init() {
		reset.write(GPIO_PIN_RESET);
		HAL_Delay(150);
		reset.write(GPIO_PIN_SET);
		HAL_Delay(10000);
		serial.write("AT\r\n");
		await_line("RDY\r\n", 10000);

		serial.write("AT+CMGF=1\r\n"); // Текстовый формат работы
		await_line("OK\r\n");
		serial.write("AT+GSMBUSY=1\r\n"); // Блокировка звонков
		await_line("OK\r\n");
		serial.write("AT+CNMI=0,0,0,0,0\r\n"); // Блокировка уведомлений смс
		await_line("OK\r\n");
		//serial.write("AT+CSCS=\"GSM\"\r\n"); // Формат сообщений
		//await_line("OK\r\n");
		read_admin_number();
		comm_timeout.start(5000);
	}

	bool await_line(std::string needle_line, uint32_t timeout = 3000) {
		Timer t(timeout);
		while(!t.done()) {
			auto line = serial.readUntil("\r\n");
			if (line != "") {
				Serial2.write(line);
				if (line == needle_line)
					return true;
			}
		}
		return false;
	}

	bool change_admin_number(std::string number) {
		serial.write("AT+CPBW=100,\"");
		serial.write(number);
		serial.write("\",145,\"ADMIN\"\r\n");
		auto res = await_line("OK\r\n");
		read_admin_number();
		return res;
	}

	void read_admin_number() {
		Timer t(3000ul);
		serial.write("AT+CPBR=100\r\n"); // Прочитать номер админа.
		while(!t.done()) {
			auto line = serial.readUntil("\r\n");
			if (line != "") {
				Serial2.write(line);
				char number[16];
				if (sscanf(line.c_str(), "+CPBR: 100,\"%15[^\"]\",145,\"ADMIN\"", number) == 1) {
					control_number = std::string(number);
				} else if (line == "OK\r\n") {
					return;
				}
			}
		}
	}

	void send_sms(const std::string & message) {
		outgoing_sms.push(message);
	}


	void invalidate_code() {
		auto & id = active_commands[command_code];
		keypad_code = {{ -1, -1, -1, -1 }};
		if (id != 255) {
			delete_list.insert(id);
			id = 255;
		}
	}

	void poll_sms() {
		serial.write("AT+CMGL=\"ALL\"\r\n");
	}

	void _poll_sms() {
		Timer t(3000ul);
		serial.write("AT+CMGL=\"ALL\"\r\n");
		while(!t.done()) {
			auto line = serial.readUntil("\r\n");
			if (line != "") {
				Serial2.write(line);
				char status[32], number[32], time[32];
				Incoming_SMS sms;
				if (sscanf(line.c_str(), "+CMGL: %d,\"%31[^\"]\",\"%31[^\"]\",\"\",\"%31[^\"]\"", &sms.storage_number, status, number, time) == 4) {
					sms.phone_number = std::string(number);
					sms.time = std::string(time);
					t.start(3000);
					while(true) {
						if (t.done()) {
							return; // ??
						}
						line = serial.readUntil("\r\n");
						if (line != "") {
							sms.line = line;
							on_sms(sms);
							t.start(3000);
							break;
						}
					}
				} else if (line == "OK\r\n") {
					return;
				}
			}
		}
	}

	void parser_loop() {
		auto line = serial.readUntil("\r\n");
		if (line != "") {
			comm_timeout.start(2000);
			Serial2.write("unacc: ");
			Serial2.write(line);
			switch (parser_state) {

			case waiting_for_sms:
				parser_state = start;
				temp_sms.line = line;
				on_sms(temp_sms);
				//sscanf(line.c_str(), "CODE %d", &num);
				timeout.start(200);
				break;

			case start:
				char status[32], number[32], time[32], name[32];
				if (sscanf(line.c_str(), "+CMGL: %d,\"%31[^\"]\",\"%31[^\"]\",%31[^,],\"%31[^\"]\"", &temp_sms.storage_number, status, number, name, time) == 5) {
					temp_sms.phone_number = std::string(number);
					temp_sms.time = std::string(time);
					/*if (phone_number != control_number) {
						delete_list.insert(temp_sms.storage_number);
						return;
					}*/
					parser_state = waiting_for_sms;
					timeout.start(200);
				} else if (sscanf(line.c_str(), "+CMTI: %s", status) == 1) {
					poll_sms();
				}
				break;
			}
		}
		if (timeout.done()) {
			parser_state = start;
		}
		if (comm_timeout.done()) {
			auto it = delete_list.begin();
			if (it != delete_list.end()) {
				_delete_sms(*it);
				delete_list.erase(it);
			} else if (!outgoing_sms.empty()) {
				auto message = outgoing_sms.front();
				outgoing_sms.pop();
				_send_sms(message);
			} else {
				poll_sms();
			}
			comm_timeout.start(2000);
		}
	}


	void on_sms(Incoming_SMS sms) {
		sms.print();
		//if (sms.phone_number != ...) sms.delete();
		char code[4];
		std::array<int8_t, 4> temp_code;
		if (sscanf(sms.line.c_str(), "CODE %4s", code) == 1) {
			if (active_commands[command_code] != sms.storage_number) {
				for(auto i = 0; i < 4; i++) {
					if (code[i] == 0 || code[i] < '0' || code[i] > '9') {
						send_sms("WRONG CODE FORMAT");
						return;
					}
					temp_code[i] = code[i] - '0';
				}
				char buf[32];
				sprintf(buf, "CODE IS %d%d%d%d", temp_code[0], temp_code[1], temp_code[2], temp_code[3]);
				send_sms(buf);
				std::copy(temp_code.begin(), temp_code.end(), keypad_code.begin());
				if (active_commands[command_code] != 255) {
					delete_list.insert(active_commands[command_code]);
				}
			}
			active_commands[command_code] = sms.storage_number;
			return;
		} else {
			Serial2.write("can't parse\r\n");
			delete_list.insert(sms.storage_number);
			return;
		}
	}
};

// +CMTI: "SM",1 -- пришла смс
// AT+CMGL="ALL" -- Показать смс в хранилище.
// AT+CMGD=7,0 -- Удалить сообщение под индексом 7
// AT+CPBR=1,255 -- прочитать всю адресную книгу
// AT+CPBW=1 -- стереть запись 1
// AT+CPBW=100,"+79168740930",145,"ADMIN"
