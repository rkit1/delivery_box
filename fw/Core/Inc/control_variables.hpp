#pragma once
#include <array>
#include <vector>
#include "keypad.h"
#include <stdint.h>
#include "Timer.hpp"

std::array<int8_t, 4> keypad_code = {{ -1, -1, -1, -1 }};
std::string control_number = "+79168740930";
constexpr bool sms_debug = 0;
