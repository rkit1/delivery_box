#pragma once
#include "main.h"
#include <array>
#include <gpio.hpp>

template <typename T, size_t X, size_t Y> using array2d = std::array<std::array<T, X>, Y>;

	//constexpr std::array<uint16_t, 3> cols = {{ K_COL1_Pin, K_COL2_Pin, K_COL3_Pin }};
	//constexpr std::array<uint16_t, 4> rows = {{ K_ROW1_Pin, K_ROW2_Pin, K_ROW3_Pin, K_ROW4_Pin }};
//	constexpr std::array<int8_t, 12> key_map = {{ 1,4,7,10,2,5,8,0,3,6,9,11 }};

std::array<GPIO, 3> cols = {{ _GPIO(K_COL1), _GPIO(K_COL2), _GPIO(K_COL3) }};
std::array<GPIO, 4> rows = {{ _GPIO(K_ROW1), _GPIO(K_ROW2), _GPIO(K_ROW3), _GPIO(K_ROW4) }};

constexpr array2d<int8_t, 3, 4> key_map = {{ {{  1,  2,  3 }},
											 {{  4,  5,  6 }},
											 {{  7,  8,  9 }},
											 {{ 10,  0, 11 }} }};


class Keypad {
public:
	static constexpr int8_t key_enter = 10;
	static constexpr int8_t key_reset = 11;

	static int8_t read() {
		for(uint8_t i = 0; i < cols.size(); i++) {
			cols[i].write(GPIO_PIN_RESET);
			//for(uint8_t j = 0; j < 10; j++) asm("nop");
			for(uint8_t j = 0; j < rows.size(); j++) {
				if(rows[j].read() == GPIO_PIN_RESET) {
					cols[i].write(GPIO_PIN_SET);
					return key_map[j][i];
				}
			}
			cols[i].write(GPIO_PIN_SET);
		}
		return -1;
	}

	static int8_t debounce(int8_t code) {
		static int8_t old_code = -1;
		static uint32_t ticks;
		if (code == old_code) {
			ticks = HAL_GetTick();
			return -1;
		} else if ((HAL_GetTick() - ticks) > 30) {
			ticks = HAL_GetTick();
			old_code = code;
			return code;
		} else {
			return -1;
		}
	}

	static void init() {
		for(auto & col: cols) {
			col.write(GPIO_PIN_SET);
		}
	}

	static void prepare_stop() {
		for(auto & col: cols) {
			col.write(GPIO_PIN_RESET);
		}
	}
};
