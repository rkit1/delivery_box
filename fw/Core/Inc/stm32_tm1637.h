#pragma once
#include <array>

void tm1637Init(void);
void tm1637DisplayDecimal(int v, int displaySeparator);
void tm1637DisplayDecimal(int v);
void tm1637DisplayArr(std::array<int8_t, 4> data);
void tm1637SetBrightness(char brightness);
void tm1637Clear();
