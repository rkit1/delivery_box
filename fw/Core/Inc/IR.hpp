#pragma once
#include "adc_group_manager.hpp"
#include "stdint.h"
#include "stm32f0xx_hal.h"
#include "main.h"
#include <array>
#include "Serial.hpp"

extern SerialClass Serial2;
extern ADC_HandleTypeDef hadc;

namespace IR {

constexpr std::array<uint16_t, 4> ir_led_pins = {{IR_LED0_Pin, IR_LED1_Pin, IR_LED2_Pin, IR_LED3_Pin}};
const std::vector<uint8_t> ir_adc_channels = {{ADC_CHANNEL_0, ADC_CHANNEL_1, ADC_CHANNEL_4, ADC_CHANNEL_8}};
ADC_group ir_adc_group(ir_adc_channels);

void scan_ir(int16_t analog[4]) {
	HAL_ADC_Start_DMA(&hadc, (uint32_t*)analog, 4);
	while(hadc.DMA_Handle->State == HAL_DMA_STATE_BUSY);
}

void sort_ir(int16_t analog[4]) {
	auto temp = analog[0];
	analog[0] = analog[3];
	analog[3] = analog[1];
	analog[1] = analog[2];
	analog[2] = temp;
}

void print_ir(int16_t analog[4]) {
	char buf[64];
	sprintf(buf, "%d\t%d\t%d\t%d\n", analog[0],analog[1],analog[2],analog[3]);
	Serial2.write(buf);
}


void debug_ir() {
	ir_adc_group.enable();
	int16_t baseline[4];
	int16_t analog[4];
	int16_t out[4] = {0};
	scan_ir(baseline);
	for(int p = 0; p < 4; ++p) {
		auto pin = ir_led_pins[p];
		HAL_GPIO_WritePin(IR_LED0_GPIO_Port, pin, GPIO_PIN_SET);
		for (int j = 0; j < 400; ++j) __asm__ __volatile__("nop\n\t":::"memory");
		scan_ir(analog);
		HAL_GPIO_WritePin(IR_LED0_GPIO_Port, pin, GPIO_PIN_RESET);
		for (int j = 0; j < 1400; ++j) __asm__ __volatile__("nop\n\t":::"memory");
		for (int j = 0; j < 4; ++j) out[p] += baseline[j] - analog[j];
	}

	print_ir(out);

	uint16_t digits = 0;
	for (int j = 0; j < 4; ++j) {
		digits *= 10;
		if (out[j] < 250) {
			digits += 8;
		}
	}
}

bool check_for_object(int16_t threshold = 250) {
	ir_adc_group.enable();
	int16_t baseline[4];
	int16_t analog[4];
	scan_ir(baseline);
	//print_ir(baseline);
	for(int p = 0; p < 4; ++p) {
		int16_t out = 0;
		auto pin = ir_led_pins[p];
		HAL_GPIO_WritePin(IR_LED0_GPIO_Port, pin, GPIO_PIN_SET);
		for (int j = 0; j < 400; ++j) __asm__ __volatile__("nop\n\t":::"memory");
		scan_ir(analog);
		//print_ir(analog);
		HAL_GPIO_WritePin(IR_LED0_GPIO_Port, pin, GPIO_PIN_RESET);
		for (int j = 0; j < 1400; ++j) __asm__ __volatile__("nop\n\t":::"memory");
		for (int j = 0; j < 4; ++j) out += baseline[j] - analog[j];
		/*char buf[32];
		sprintf(buf, "%d\n", out);
		Serial2.write(buf);*/
		if (out < threshold) return true;
	}
	return false;
}

}
