#pragma once
#include "stm32f0xx_hal.h"
#include <vector>
#include <functional>
extern ADC_HandleTypeDef hadc;

class ADC_group {
	static uint8_t counter;
	static ADC_group * current_group;
	const std::vector<uint8_t> channels;
	void clear_channels() {
		ADC_ChannelConfTypeDef sConfig = {0};
		sConfig.Rank = ADC_RANK_NONE;
		for (auto c: channels) {
			sConfig.Channel = c;
			HAL_ADC_ConfigChannel(&hadc, &sConfig);
		}
	}
	void setup_channels() {
		HAL_ADC_DeInit(&hadc);
		hadc.Init.Resolution = resolution;
		HAL_ADC_Init(&hadc);
		ADC_ChannelConfTypeDef sConfig = {0};
		sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
		sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
		for (auto c: channels) {
			sConfig.Channel = c;
			HAL_ADC_ConfigChannel(&hadc, &sConfig);
		}
	}
	uint8_t id;
	uint32_t resolution;
public:
	ADC_group(const std::vector<uint8_t> channels, uint32_t resolution = ADC_RESOLUTION_8B): channels(channels), resolution(resolution) {
		id = counter++;
	}
	void enable() {
		if (ADC_group::current_group->id != id) {
			ADC_group::current_group->clear_channels();
			setup_channels();
			ADC_group::current_group = this;
		}
	}
};


ADC_group all_channels(std::vector<uint8_t>({{ADC_CHANNEL_0, ADC_CHANNEL_1, ADC_CHANNEL_4, ADC_CHANNEL_8, ADC_CHANNEL_11}}));
ADC_group * ADC_group::current_group = &all_channels;
uint8_t ADC_group::counter = 0;
