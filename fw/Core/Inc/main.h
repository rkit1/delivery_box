/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define ADC_IN10_Pin GPIO_PIN_0
#define ADC_IN10_GPIO_Port GPIOC
#define BAT_LEVEL_Pin GPIO_PIN_1
#define BAT_LEVEL_GPIO_Port GPIOC
#define SIM800_RESET_Pin GPIO_PIN_3
#define SIM800_RESET_GPIO_Port GPIOC
#define PHOTO_IN3_Pin GPIO_PIN_0
#define PHOTO_IN3_GPIO_Port GPIOA
#define PHOTO_IN2_Pin GPIO_PIN_1
#define PHOTO_IN2_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define PHOTO_IN1_Pin GPIO_PIN_4
#define PHOTO_IN1_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define BOX_OPEN_Pin GPIO_PIN_6
#define BOX_OPEN_GPIO_Port GPIOA
#define K_ROW3_Pin GPIO_PIN_5
#define K_ROW3_GPIO_Port GPIOC
#define PHOTO_IN0_Pin GPIO_PIN_0
#define PHOTO_IN0_GPIO_Port GPIOB
#define IR_LED3_Pin GPIO_PIN_1
#define IR_LED3_GPIO_Port GPIOB
#define ADMIN_BTN_Pin GPIO_PIN_11
#define ADMIN_BTN_GPIO_Port GPIOB
#define DISP_CLK_Pin GPIO_PIN_12
#define DISP_CLK_GPIO_Port GPIOB
#define IR_LED0_Pin GPIO_PIN_13
#define IR_LED0_GPIO_Port GPIOB
#define IR_LED1_Pin GPIO_PIN_14
#define IR_LED1_GPIO_Port GPIOB
#define IR_LED2_Pin GPIO_PIN_15
#define IR_LED2_GPIO_Port GPIOB
#define K_COL3_Pin GPIO_PIN_6
#define K_COL3_GPIO_Port GPIOC
#define K_COL2_Pin GPIO_PIN_8
#define K_COL2_GPIO_Port GPIOC
#define K_COL1_Pin GPIO_PIN_9
#define K_COL1_GPIO_Port GPIOC
#define DISP_DIO_Pin GPIO_PIN_11
#define DISP_DIO_GPIO_Port GPIOA
#define K_ROW4_Pin GPIO_PIN_12
#define K_ROW4_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define LOCK_OPEN_Pin GPIO_PIN_7
#define LOCK_OPEN_GPIO_Port GPIOB
#define K_ROW1_Pin GPIO_PIN_8
#define K_ROW1_GPIO_Port GPIOB
#define K_ROW2_Pin GPIO_PIN_9
#define K_ROW2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
