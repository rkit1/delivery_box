#pragma once
#include "stm32f0xx_hal.h"

class Timer {
    public:
    unsigned long started_at = 0;
    unsigned long interval = 0;
    bool running = true;
    Timer() {}
    Timer(unsigned long timeout) {
    	start(timeout);
    }
    Timer(bool running): running(running){}
    void start(unsigned long _interval) {
        started_at = HAL_GetTick();
        interval = _interval;
        running = true;
    }
    void start_loop(unsigned long _interval) {
        started_at += interval;
        interval = _interval;
        running = true;
    }
    bool done() {
        if (running && ((HAL_GetTick() - started_at) > interval)) {
            running = false;
            return true;
        }
        return false;
    }
};
