#pragma once
#include <stm32f0xx_hal.h>
#include <stdint.h>

class GPIO {
	uint16_t pin;
	GPIO_TypeDef * port;
public:
	GPIO_PinState front_state = (GPIO_PinState)2;

	GPIO(uint16_t pin, GPIO_TypeDef * port): pin(pin), port(port){};
	inline void write(GPIO_PinState val) {
		HAL_GPIO_WritePin(port, pin, val);
	}
	inline GPIO_PinState read(){
		return HAL_GPIO_ReadPin(port, pin);
	}


	inline bool front() {
		auto r = this->read();
		if (front_state != r) {
			front_state = r;
			return true;
		}
		return false;
	}
	inline bool front(GPIO_PinState state) {
		return front() && front_state == state;
	}
};




#define _GPIO(x) (GPIO(x##_Pin, x##_GPIO_Port))

